import sys

model_file_name = sys.argv[1]
testing_file_name = sys.argv[2]

sppo_token_dict = {}
sppo_token_dict_smoothed = {}
hane_token_dict = {}
hane_token_dict_smoothed = {}


# Open model file, read in necessary stats
with open(model_file_name,"r",encoding="latin1") as model_file:
	
	email_imdb = model_file.readline().strip()
	if email_imdb == "EMAIL DATA":
		data = "email"
	else: # email_imdb == "IMDB DATA"
		data = "imdb"
	#END if
	
	# Log of P(SPAM) or P(POSITIVE)
	log_p_sppo = float(model_file.readline().strip())
	# Log of P(HAM) or P(NEGATIVE)
	log_p_hane = float(model_file.readline().strip())
	
	# Log of P(TOKEN|SPAM) or P(TOKEN|POSITIVE)
	log_p_unseen_token_sppo = float(model_file.readline().strip())
	# Log of P(TOKEN|HAM) or P(TOKEN|NEGATIVE)
	log_p_unseen_token_hane = float(model_file.readline().strip())
	
	# VOCAB SIZE
	vocab_size = int(model_file.readline().strip())
	# SPAM / POSITIVE DICTIONARY SIZE
	sppo_dict_len = int(model_file.readline().strip())
	# HAM / NEGATIVE DICTIONARY SIZE
	hane_dict_len = int(model_file.readline().strip())
	
	for sppo_read_count in range(0,sppo_dict_len):
		
		sppo_vocab_entry = model_file.readline().strip().split(":")
		
		sppo_token_id = int(sppo_vocab_entry[0])
		log_p_token_sppo = float(sppo_vocab_entry[1])
		log_p_token_sppo_smoothed = float(sppo_vocab_entry[2])
		
		sppo_token_dict[sppo_token_id] = log_p_token_sppo
		sppo_token_dict_smoothed[sppo_token_id] = log_p_token_sppo_smoothed
		
	#END for
	
	for hane_read_count in range(0,hane_dict_len):
		
		hane_vocab_entry = model_file.readline().strip().split(":")
		
		hane_token_id = int(hane_vocab_entry[0])
		log_p_token_hane = float(hane_vocab_entry[1])
		log_p_token_hane_smoothed = float(hane_vocab_entry[2])
		
		hane_token_dict[hane_token_id] = log_p_token_hane
		hane_token_dict_smoothed[hane_token_id] = log_p_token_hane_smoothed
		
	#END for
#END with
model_file.close()

# Open testing_file, go through each line, print SPAM/HAM or POSITIVE/NEGATIVE
with open(testing_file_name,"r",encoding="latin1") as testing_file:
	
	for line in testing_file:
		
		sum_of_logs_sppo = 0
		sum_of_logs_hane = 0
		
		test_token_data_list =  line.split()
		
		for test_token_data in test_token_data_list:
			
			test_token = test_token_data.split(":")
			test_token_id = int(test_token[0])
			test_token_count = int(test_token[1])
			
			# Calculate sum of logs for SPAM / POSITIVE
			if test_token_id in sppo_token_dict_smoothed:
				token_log_times_count_sppo = sppo_token_dict_smoothed[test_token_id] * test_token_count
			else:
				token_log_times_count_sppo = log_p_unseen_token_sppo * test_token_count
			#END if
			sum_of_logs_sppo += token_log_times_count_sppo
			
			# Calculate sum of logs for HAM / NEGATIVE
			if test_token_id in hane_token_dict_smoothed:
				token_log_times_count_hane = hane_token_dict_smoothed[test_token_id] * test_token_count
			else:
				token_log_times_count_hane = log_p_unseen_token_hane * test_token_count
			#END if
			sum_of_logs_hane += token_log_times_count_hane
			
		#END for
		
		#print("sum_of_logs_sppo: " + str(sum_of_logs_sppo))
		#print("sum_of_logs_hane: " + str(sum_of_logs_hane))
		
		if sum_of_logs_sppo >= sum_of_logs_hane:
			if data == "email":
				print("SPAM")
			else: # data == "imdb"
				print("POSITIVE")
			#END if
		else: # sum_of_logs_sppo < sum_of_logs_hane
			if data == "email":
				print("HAM")
			else: # data == "imdb"
				print("NEGATIVE")
			#END if
		#END if
		
	#END for
#END with
testing_file.close()
