import collections
import random
import os

token_dictionary = {}
identifier = 1

HW2_DIRECTORY = os.path.abspath(os.path.dirname(__file__))
ENRON1_DIRECTORY = os.path.join(HW2_DIRECTORY, "enron1")
ENRON2_DIRECTORY = os.path.join(HW2_DIRECTORY, "enron2")
ENRON4_DIRECTORY = os.path.join(HW2_DIRECTORY, "enron4")
ENRON5_DIRECTORY = os.path.join(HW2_DIRECTORY, "enron5")
UNLABELLED_DATA_DIRECTORY = os.path.join(HW2_DIRECTORY, "spam_or_ham_test")

def process_files_by_directory(dir_path, op_file, svm_op_file, megam_op_file):
	
	SPAM_SUBDIRECTORY = os.path.join(dir_path, "spam")
	HAM_SUBDIRECTORY = os.path.join(dir_path, "ham")
	
	for subdirs, dirs, files in os.walk(SPAM_SUBDIRECTORY):
		for spam_file in files:
			spam_curr_file_path = os.path.join(SPAM_SUBDIRECTORY,spam_file)
			op_file.write("SPAM")
			svm_op_file.write("+1")
			megam_op_file.write("1")
			process_file(spam_curr_file_path, op_file, svm_op_file, megam_op_file)
		#END for
	#END for
			
	for ham_subdirs, ham_dirs, ham_files in os.walk(HAM_SUBDIRECTORY):
		for ham_file in ham_files:
			ham_curr_file_path = os.path.join(HAM_SUBDIRECTORY,ham_file)
			op_file.write("HAM")
			svm_op_file.write("-1")
			megam_op_file.write("0")
			process_file(ham_curr_file_path, op_file, svm_op_file, megam_op_file)
		#END for
	#END for
	
#END def process_files_by_directory


def process_file(file_path, op_file, svm_op_file, megam_op_file):
	global token_dictionary
	global identifier
	
	frequency_dictionary = {}
	
	with open(file_path,"r",encoding="latin1") as ip_file:
		
		for line in ip_file:
			tokens = []
			tokens = line.split()
			
			for token in tokens:
				token_id = token_dictionary[token]
					
				if token_id in frequency_dictionary:
					frequency_dictionary[token_id] += 1
				else:
					frequency_dictionary[token_id] = 1
				#END if
				
			#END for
		#END for
		
		ordered_frequency_dictionary = collections.OrderedDict(sorted(frequency_dictionary.items()))
		
		for key in ordered_frequency_dictionary.keys():
			op_file.write(" " + str(key) + ":" + str(ordered_frequency_dictionary[key]))
			svm_op_file.write(" " + str(key) + ":" + str(ordered_frequency_dictionary[key]))
			megam_op_file.write(" " + str(key) + " " + str(ordered_frequency_dictionary[key]))
		#END for
		
		op_file.write("\n")
		svm_op_file.write("\n")
		megam_op_file.write("\n")
		
	#END with
	ip_file.close()
	
#END def process_file

if __name__ == '__main__':
	
	vocab_file_path = os.path.join(HW2_DIRECTORY,"enron.vocab")
	output_file_path = os.path.join(HW2_DIRECTORY,"input_email_nb")
	svm_output_file_path = os.path.join(HW2_DIRECTORY,"input_email_svm")
	megam_output_file_path = os.path.join(HW2_DIRECTORY,"input_email_megam")
	unlabelled_data_file_path_nb = os.path.join(HW2_DIRECTORY,"test_email_nb")
	unlabelled_data_file_path_svm = os.path.join(HW2_DIRECTORY,"test_email_svm")
	unlabelled_data_file_path_megam = os.path.join(HW2_DIRECTORY,"test_email_megam")
	
	index = 1
	with open(vocab_file_path,"r",encoding="latin1") as vocab_file:
		for line in vocab_file:
			stripped_line = line.strip()
			token_dictionary[stripped_line] = index
			index += 1
		#END for
	#END with
	vocab_file.close()
	
	
	with open(megam_output_file_path,"w",encoding="latin1") as megam_op_file:
		with open(svm_output_file_path,"w",encoding="latin1") as svm_op_file:
			with open(output_file_path,"w",encoding="latin1") as op_file:
				process_files_by_directory(ENRON1_DIRECTORY, op_file, svm_op_file, megam_op_file)
				process_files_by_directory(ENRON2_DIRECTORY, op_file, svm_op_file, megam_op_file)
				process_files_by_directory(ENRON4_DIRECTORY, op_file, svm_op_file, megam_op_file)
				process_files_by_directory(ENRON5_DIRECTORY, op_file, svm_op_file, megam_op_file)
			#END with
			op_file.close()
		#END with
		svm_op_file.close()
	#END with
	megam_op_file.close()
	
	
	test_token_dictionary = {}
	#id_counter = 1
	
	with open(unlabelled_data_file_path_megam,"w",encoding="latin1") as ul_megam_file:
		with open(unlabelled_data_file_path_svm,"w",encoding="latin1") as ul_svm_file:
			with open(unlabelled_data_file_path_nb,"w",encoding="latin1") as ul_nb_file:
				
				for test_subdirs, test_dirs, test_files in os.walk(UNLABELLED_DATA_DIRECTORY):
					for test_file in sorted(test_files):
						#print(test_file)
						test_curr_file_path = os.path.join(UNLABELLED_DATA_DIRECTORY,test_file)
						
						test_frequency_dictionary = {}
						with open(test_curr_file_path,"r",encoding="latin1") as curr_test_file:
							
							for line in curr_test_file:
								tokens = []
								tokens = line.split()
								
								for token in tokens:
									# made last minute changes here
									# ignored unseen tokens
									# was extremely skewed => 9000:2500, 7000:4500, 11000:500
									# has become a lot even => 7000:4500, 6250:5250 6000:5500
									#if token in test_token_dictionary:
									if token in token_dictionary:
										#token_id = test_token_dictionary[token]
										token_id = token_dictionary[token]
									#else:
										#test_token_dictionary[token] = id_counter
										#token_id = id_counter
										#id_counter += 1
										
										if token_id in test_frequency_dictionary:
											test_frequency_dictionary[token_id] += 1
										else:
											test_frequency_dictionary[token_id] = 1
										#END if
									
								#END for
							#END for
							
							ordered_test_frequency_dictionary = collections.OrderedDict(sorted(test_frequency_dictionary.items()))
							
							ul_file_write_string = ""
							ul_megam_temp_string = ""
							for key in ordered_test_frequency_dictionary.keys():
								line_string = str(key) + ":" + str(ordered_test_frequency_dictionary[key]) + " "
								ul_file_write_string += line_string
								megam_line_string = str(key) + " " + str(ordered_test_frequency_dictionary[key]) + " "
								ul_megam_temp_string += megam_line_string
							#END for
							
							ul_nb_string = ul_file_write_string.strip()
							ul_nb_file.write(ul_nb_string)
							ul_nb_file.write("\n")
							
							num = random.randint(1,100)
							if num >= 50:
								ul_svm_string = "+1 " + ul_nb_string
								ul_megam_string = "1 " + ul_megam_temp_string.strip()
							else: # num < 50
								ul_svm_string = "-1 " + ul_nb_string
								ul_megam_string = "0 " + ul_megam_temp_string.strip()
							#END if
							
							ul_svm_file.write(ul_svm_string)
							ul_svm_file.write("\n")
							
							ul_megam_file.write(ul_megam_string)
							ul_megam_file.write("\n")
							
						#END with
						curr_test_file.close()
						
					#END for
				#END for
				
			#END with
			ul_nb_file.close()
		#END with
		ul_svm_file.close()
	#END with
	ul_megam_file.close()
	
#END __main__