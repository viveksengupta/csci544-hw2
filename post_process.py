import sys

ip_file_name = sys.argv[1]
op_file_name = ip_file_name + ".out"
email_imdb = sys.argv[2]
svm_megam = sys.argv[3]


with open(ip_file_name,"r",encoding="latin1") as ip_file:
	with open(op_file_name,"w",encoding="latin1") as op_file:
		
		if email_imdb == "email":
			for line in ip_file:
				if svm_megam == "svm":
					if float(line.strip()) >= 0:
						op_file.write("SPAM" + "\n")
					else: # float(line.strip()) < 0
						op_file.write("HAM" + "\n")
					#END if
				else: # svm_megam == "megam"
					class_tag = line.split()[0]
					if class_tag == "1":
						op_file.write("SPAM" + "\n")
					else: # class_tag == "0"
						op_file.write("HAM" + "\n")
					#END if
				#END if
			#END for
		else:
			for line in ip_file:
				if svm_megam == "svm":
					if float(line.strip()) >= 0:
						op_file.write("POSITIVE" + "\n")
					else: # line.strip() == hane_num
						op_file.write("NEGATIVE" + "\n")
					#END if
				else: # svm_megam == "megam"
					class_tag = line.split()[0]
					if class_tag == "1":
						op_file.write("POSITIVE" + "\n")
					else: # class_tag == "0"
						op_file.write("NEGATIVE" + "\n")
					#END if
				#END if
				#END if
			#END for
		#END if
		
	#END with
	op_file.close()
#END with
ip_file.close()