import random
import re

regex = re.compile("([^\s]*)\s(.*)")

input_file_name = "labeledBow.feat.fixed"
output_file_name_nb = "input_imdb_nb"
output_file_name_svm = "input_imdb_svm"
output_file_name_megam = "input_imdb_megam"
unlabelled_data_file_name = "sentiment_test.feat.fixed"
test_nb_file_name = "test_imdb_nb"
test_svm_file_name = "test_imdb_svm"
test_megam_file_name = "test_imdb_megam"


with open(output_file_name_megam,"w",encoding="latin1") as megam_op_file:
	with open(output_file_name_svm,"w",encoding="latin1") as svm_op_file:
		with open(output_file_name_nb,"w",encoding="latin1") as op_file:
			with open(input_file_name,"r",encoding="latin1") as ip_file:
				
				for ip_line in ip_file:
					match = regex.match(ip_line)
					
					rating = int(match.group(1))
					tokens_ids_counts = match.group(2).split()
					
					if rating >= 7:
						op_file.write("POSITIVE")
						svm_op_file.write("+1")
						megam_op_file.write("1")
					elif rating <= 4:
						op_file.write("NEGATIVE")
						svm_op_file.write("-1")
						megam_op_file.write("0")
					else:
						print("rating >4 and <7 - not accepted")
						continue
					#END if
					
					for token_id_count in tokens_ids_counts:
						old_id_count = token_id_count.split(":")
						
						old_id = int(old_id_count[0])
						old_count = old_id_count[1]
						
						new_id = old_id + 1
						new_id_count = str(new_id) + ":" + old_count
						new_id_count_megam = str(new_id) + " " + old_count
						
						op_file.write(" " + new_id_count)
						svm_op_file.write(" " + new_id_count)
						megam_op_file.write(" " + new_id_count_megam)
					#END for
					
					op_file.write("\n")
					svm_op_file.write("\n")
					megam_op_file.write("\n")
				#END for
				
			#END with
			ip_file.close()
		#END with
		op_file.close()
	#END with
	svm_op_file.close()
#END with
megam_op_file.close()


ul_nb_file = open(test_nb_file_name,"w",encoding="latin1")
ul_svm_file = open(test_svm_file_name,"w",encoding="latin1")
ul_megam_file = open(test_megam_file_name,"w",encoding="latin1")

with open(unlabelled_data_file_name,"r",encoding="latin1") as ul_file:
	
	for ul_line in ul_file:
		ul_tokens_ids_counts = ul_line.strip().split()
		
		curr_line_string = ""
		curr_line_string_megam = ""
		for ul_token_id_count in ul_tokens_ids_counts:
			ul_old_id_count = ul_token_id_count.split(":")
			
			ul_old_id = int(ul_old_id_count[0])
			ul_old_count = ul_old_id_count[1]
			
			ul_new_id = ul_old_id + 1
			ul_new_id_count = str(ul_new_id) + ":" + ul_old_count
			ul_new_id_count_megam = str(ul_new_id) + " " + ul_old_count
			
			curr_line_string += ul_new_id_count + " "
			curr_line_string_megam += ul_new_id_count_megam + " "
		#END for
		
		ul_nb_string = curr_line_string.strip()
		ul_nb_file.write(ul_nb_string)
		ul_nb_file.write("\n")
		
		ul_megam_string_temp = curr_line_string_megam.strip()
		
		num = random.randint(1,100)
		if num >= 50:
			ul_svm_string = "+1 " + ul_nb_string
			ul_megam_string = "1 " + ul_megam_string_temp
		else: # num < 50
			ul_svm_string = "-1 " + ul_nb_string
			ul_megam_string = "0 " + ul_megam_string_temp
		#END if
		ul_svm_file.write(ul_svm_string)
		ul_svm_file.write("\n")
		ul_megam_file.write(ul_megam_string)
		ul_megam_file.write("\n")
	#END for
	
#END with
ul_file.close()

ul_nb_file.close()
ul_svm_file.close()
ul_megam_file.close()