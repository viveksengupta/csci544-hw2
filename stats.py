import sys

results_file_name = sys.argv[1]
labels_file_name = sys.argv[2]

with open(results_file_name,"r",encoding="latin1") as results_file:
	with open(labels_file_name,"r",encoding="latin1") as labels_file:
		
		labels_first_line = labels_file.readline().strip()
		if labels_first_line == "SPAM" or labels_first_line == "HAM":
			class_0 = "SPAM"
			class_1 = "HAM"
		else:
			class_0 = "POSITIVE"
			class_1 = "NEGATIVE"
		#END if
		labels_file.seek(0,0)
		
		class_0_count = 0
		class_1_count = 0
		class_0_correct = 0
		class_1_correct = 0
		class_0_classified = 0
		class_1_classified = 0
		
		for results_line in results_file:
			labels_line = labels_file.readline()
			
			if labels_line.strip() == class_0:
				class_0_count += 1
				if results_line.strip() == class_0:
					class_0_correct += 1
					class_0_classified += 1
				else:
					class_1_classified += 1
				#END if
					
			else:
				class_1_count += 1
				if results_line.strip() == class_1:
					class_1_correct += 1
					class_1_classified += 1
				else:
					class_0_classified += 1
				#END if
			#END if
		#END for
		
	#END with
#END with

accuracy = (class_0_correct + class_1_correct) / (class_0_count + class_1_count)
print("accuracy: " + str(accuracy))
print()

precision_0 = class_0_correct / class_0_classified
precision_1 = class_1_correct / class_1_classified
print("precision_0: " + str(precision_0))
print("precision_1: " + str(precision_1))
print()

recall_0 = class_0_correct / class_0_count
recall_1 = class_1_correct / class_1_count
print("recall_0: " + str(recall_0))
print("recall_1: " + str(recall_1))
print()

fscore_0 = 2 * precision_0 * recall_0 / (precision_0 + recall_0)
fscore_1 = 2 * precision_1 * recall_1 / (precision_1 + recall_1)
print("fscore_0: " + str(fscore_0))
print("fscore_1: " + str(fscore_1))