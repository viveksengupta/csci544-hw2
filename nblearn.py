import collections
import math
import re
import sys

# Regular Expressions to determine SPAM or HAM
regex_spam = "SPAM\s(.*)"
regex_ham = "HAM\s(.*)"

# Regular Expressions to determine POSITIVE or NEGATIVE
regex_positive = "POSITIVE\s(.*)"
regex_negative = "NEGATIVE\s(.*)"

# Global Variables
spam_positive_count = 0
ham_negative_count = 0

spam_positive_word_dictionary = {}
ham_negative_word_dictionary = {}

vocab_size = 0
already_in_vocab = False

sppo_dict_len = 0
hane_dict_len = 0

c_sppo_t = 0
c_hane_t = 0

p_sppo = 0
p_hano = 0

# Run-time Arguments
training_file_path = sys.argv[1]
model_file_path = sys.argv[2]


# Function for counting tokens
def update_dictionary_and_count(is_spam_positive,string):
	
	global spam_positive_word_dictionary
	global ham_negative_word_dictionary
	global c_sppo_t
	global c_hane_t
	global vocab_size
	global already_in_vocab
	
	token_data_list = string.split()
	
	for token_data in token_data_list:
		already_in_vocab = False
		token_in_sppo_dict = False
		token_in_hane_dict = False
		
		token_id_count = token_data.split(":")
		token_id = int(token_id_count[0])
		token_count = int(token_id_count[1])
		
		if token_id in spam_positive_word_dictionary:
			token_in_sppo_dict = True
			already_in_vocab = True
		if token_id in ham_negative_word_dictionary:
			token_in_hane_dict = True
			already_in_vocab = True
			
		if is_spam_positive == 1:
			c_sppo_t += token_count
			
			#if token_id in spam_positive_word_dictionary:
			if token_in_sppo_dict == True:
				spam_positive_word_dictionary[token_id] += token_count
			else:
				spam_positive_word_dictionary[token_id] =  token_count
			#END if
			
		else: # is_spam_positive == 0
			c_hane_t += token_count
			
			#if token_id in ham_negative_word_dictionary:
			if token_in_hane_dict == True:
				ham_negative_word_dictionary[token_id] += token_count
			else:
				ham_negative_word_dictionary[token_id] =  token_count
			#END if
		#END if
		
		if already_in_vocab == False:
			vocab_size += 1
		#END if
		
	#END for
	
#END update_dictionary_and_count


def process_training_file(data):

	global training_file_path
	global total_count
	global spam_positive_count
	global ham_negative_count
	global p_sppo
	global p_hano
	global regex_spam
	global regex_ham
	global regex_positive
	global regex_negative
	
	if data == "email":
		regex_spam_positive = re.compile(regex_spam)
		regex_ham_negative = re.compile(regex_ham)
	else: #data == "imdb"
		regex_spam_positive = re.compile(regex_positive)
		regex_ham_negative = re.compile(regex_negative)
	#END if
	
	with open(training_file_path,"r",encoding="latin1") as training_file:
		for line in training_file:
			
			spam_positive_match = regex_spam_positive.match(line.strip())
			ham_negative_match = regex_ham_negative.match(line.strip())
			
			if spam_positive_match:
				spam_positive_count += 1
				spam_positive_string = spam_positive_match.group(1)
				
				update_dictionary_and_count(1,spam_positive_string)
				
			elif ham_negative_match: #ham
				ham_negative_count += 1
				ham_negative_string = ham_negative_match.group(1)
				
				update_dictionary_and_count(0,ham_negative_string)
				
			else:
				print("neither ham nor spam, line ignored")
			#END if
				
			#END if
			
		#END for
	#END with
	training_file.close()
	
	total_count = spam_positive_count + ham_negative_count
	p_sppo = spam_positive_count / total_count
	p_hano = ham_negative_count / total_count

#END def process_training_file(data)


def write_model_file(data):
	
	global model_file_path
	global vocab_dictionary
	global ordered_vocab_dictionary
	global spam_positive_word_dictionary
	global ham_negative_word_dictionary
	global vocab_size
	global sppo_dict_len
	global hane_dict_len
	
	sppo_dict_len = len(spam_positive_word_dictionary)
	hane_dict_len = len(ham_negative_word_dictionary)
	
	with open(model_file_path,"w",encoding="latin1") as model_file:
		
		if data == "email":
			model_file.write("EMAIL DATA\n")
		else: # data == "imdb"
			model_file.write("IMDB DATA\n")
		#END if
		
		sppo_tot_log = math.log(p_sppo)
		hane_tot_log = math.log(p_hano)
		
		model_file.write(str(sppo_tot_log) + "\n")
		model_file.write(str(hane_tot_log) + "\n")
		
		unseen_token_sppo_log_smoothing = math.log(1 / (c_sppo_t + vocab_size))
		unseen_token_hane_log_smoothing = math.log(1 / (c_hane_t + vocab_size))
		
		model_file.write(str(unseen_token_sppo_log_smoothing) + "\n")
		model_file.write(str(unseen_token_hane_log_smoothing) + "\n")
		
		model_file.write(str(vocab_size) + "\n")
		
		model_file.write(str(sppo_dict_len) + "\n")
		model_file.write(str(hane_dict_len) + "\n")
		
		#model_file.write("P(TOKEN|SPAM) or P(TOKEN|POSITIVE)\n")
		for sppo_key in spam_positive_word_dictionary:
			sppo_count = spam_positive_word_dictionary[sppo_key]
			sppo_prob = sppo_count / c_sppo_t
			sppo_log = math.log(sppo_prob)
			sppo_prob_smoothing = (sppo_count + 1) / (c_sppo_t + vocab_size)
			sppo_log_smoothing = math.log(sppo_prob_smoothing)
			model_file.write(str(sppo_key) + ":" + str(sppo_log) + ":" + str(sppo_log_smoothing) + "\n")
		#END for
		
		#model_file.write("P(TOKEN|HAM) or P(TOKEN|NEGATIVE)\n")
		for hane_key in ham_negative_word_dictionary:
			hane_count = ham_negative_word_dictionary[hane_key]
			hane_prob = hane_count / c_hane_t
			hane_log = math.log(hane_prob)
			hane_prob_smoothing = (hane_count + 1) / (c_hane_t + vocab_size)
			hane_log_smoothing = math.log(hane_prob_smoothing)
			hane_data = [hane_log, hane_log_smoothing]
			model_file.write(str(hane_key) + ":" + str(hane_log) + ":" + str(hane_log_smoothing) + "\n")
		#END for
			
	#END with
	model_file.close()
	
#END def write_model_file(data)


if __name__ == "__main__":

	ip_file = open(training_file_path,"r",encoding="latin1")
	line = ip_file.readline()
	if re.compile(regex_spam).match(line) != None or re.compile(regex_ham).match(line) != None:
		contents = "email"
	else: # regex_positive.match(line) == True or regex_negative.match(line) == True
		contents = "imdb"
	#END if
	ip_file.close()
	
	process_training_file(contents)
	write_model_file(contents)

#END def __main__

