import math
import random
import re
import sys

regex = re.compile("(SPAM|HAM|POSITIVE|NEGATIVE)\s(.*)")

training_percentage = int(sys.argv[2])
op_files_number = sys.argv[3]
email_imdb = sys.argv[4]

labelled_file_name = sys.argv[1]
labels_file_name = "labels_" + email_imdb + "_" + op_files_number

training_file_name_nb = "training_" + email_imdb + "_nb_" + op_files_number
testing_file_name_nb = "testing_" + email_imdb + "_nb_" + op_files_number

training_file_name_svm = "training_" + email_imdb + "_svm_" + op_files_number
testing_file_name_svm = "testing_" + email_imdb + "_svm_" + op_files_number

training_file_name_megam = "training_" + email_imdb + "_megam_" + op_files_number
testing_file_name_megam = "testing_" + email_imdb + "_megam_" + op_files_number

if email_imdb == "email":
	sppo = "SPAM"
	sppo_svm = "+1 "
	sppo_megam = "1 "
	hane = "HAM"
	hane_svm = "-1 "
	hane_megam = "0 "
else: # email_imdb == "imdb"
	sppo = "POSITIVE"
	sppo_svm = "+1 "
	sppo_megam = "1 "
	hane = "NEGATIVE"
	hane_svm = "-1 "
	hane_megam = "0 "
#END if

#print(sppo)
#print(sppo_svm)
#print(sppo_megam)

line_count = 0

curr_training_line_count = 0
curr_testing_line_count = 0

training_exhausted = False
testing_exhausted = False


with open(labelled_file_name,"r",encoding="latin1") as labelled_file:
	
	for line in labelled_file:
		line_count += 1
	#END for
	labelled_file.seek(0,0)
	
	training_line_count = math.ceil(line_count * training_percentage / 100)
	testing_line_count = line_count - training_line_count
	
	#print(training_line_count)
	#print(testing_line_count)
	
	training_file_nb = open(training_file_name_nb,"w",encoding="latin1")
	testing_file_nb = open(testing_file_name_nb,"w",encoding="latin1")
	training_file_svm = open(training_file_name_svm,"w",encoding="latin1")
	testing_file_svm = open(testing_file_name_svm,"w",encoding="latin1")
	training_file_megam = open(training_file_name_megam,"w",encoding="latin1")
	testing_file_megam = open(testing_file_name_megam,"w",encoding="latin1")
	labels_file = open(labels_file_name,"w",encoding="latin1")
	
	for line in labelled_file:
		
		random_number = random.randint(1,100)
		
		if random_number <= training_percentage:
			curr_training_line_count += 1
			training_file_nb.write(line)
			
			m = regex.match(line)
			label = m.group(1)
			data = m.group(2)
			
			data_list = data.split()
			new_data_string = ""
			for data_member in data_list:
				member_id_count = data_member.split(":")
				new_data_member = member_id_count[0] + " " + member_id_count[1]
				new_data_string += new_data_member + " "
			#END for
			
			if label == "SPAM" or label == "POSITIVE":
				training_file_svm.write(sppo_svm + data + "\n")
				training_file_megam.write(sppo_megam + new_data_string.strip() + "\n")
			else: # label == "HAM"
				training_file_svm.write(hane_svm + data + "\n")
				training_file_megam.write(hane_megam + new_data_string.strip() + "\n")
			#END if
			
		else:
			curr_testing_line_count += 1
			
			m = regex.match(line)
			label = m.group(1)
			data = m.group(2)
			
			labels_file.write(label + "\n")
			testing_file_nb.write(data + "\n")
			
			
			data_list = data.split()
			new_data_string = ""
			for data_member in data_list:
				member_id_count = data_member.split(":")
				new_data_member = member_id_count[0] + " " + member_id_count[1]
				new_data_string += new_data_member + " "
			#END for
			
			if label == "SPAM" or label == "POSITIVE":
				testing_file_svm.write(sppo_svm + data + "\n")
				testing_file_megam.write(sppo_megam + new_data_string.strip() + "\n")
			else: # label == "HAM"
				testing_file_svm.write(hane_svm + data + "\n")
				testing_file_megam.write(hane_megam + new_data_string.strip() + "\n")
			#END if
		#END if
		
		if curr_training_line_count == training_line_count:
			training_exhausted = True
			break
		#END if
		
		if curr_testing_line_count == testing_line_count:
			testing_exhausted = True
			break
		#END if
			
	#END for
	
	if testing_exhausted == True:
		#all others would be training_file_nb
		for line in labelled_file:
			curr_training_line_count += 1
			training_file_nb.write(line)
			
			m = regex.match(line)
			label = m.group(1)
			data = m.group(2)
			
			data_list = data.split()
			new_data_string = ""
			for data_member in data_list:
				member_id_count = data_member.split(":")
				new_data_member = member_id_count[0] + " " + member_id_count[1]
				new_data_string += new_data_member + " "
			#END for
			
			if label == "SPAM" or label == "POSITIVE":
				training_file_svm.write(sppo_svm + data + "\n")
				training_file_megam.write(sppo_megam + new_data_string.strip() + "\n")
			else: # label == "HAM"
				training_file_svm.write(hane_svm + data + "\n")
				training_file_megam.write(hane_megam + new_data_string.strip() + "\n")
			#END if
		#END for
	#END if
	
	if training_exhausted == True:
		#all others would be testing_file
		for line in labelled_file:
			curr_testing_line_count += 1
			
			m = regex.match(line)
			label = m.group(1)
			data = m.group(2)
			
			labels_file.write(label + "\n")
			testing_file_nb.write(data + "\n")
			
			data_list = data.split()
			new_data_string = ""
			for data_member in data_list:
				member_id_count = data_member.split(":")
				new_data_member = member_id_count[0] + " " + member_id_count[1]
				new_data_string += new_data_member + " "
			#END for
			
			if label == "SPAM" or label == "POSITIVE":
				testing_file_svm.write(sppo_svm + data + "\n")
				testing_file_megam.write(sppo_megam + new_data_string.strip() + "\n")
			else: # label == "HAM"
				testing_file_svm.write(hane_svm + data + "\n")
				testing_file_megam.write(hane_megam + new_data_string.strip() + "\n")
			#END if
		#END for
	#END if
	
	training_file_nb.close()
	testing_file_nb.close()
	training_file_svm.close()
	testing_file_svm.close()
	training_file_megam.close()
	testing_file_megam.close()
	labels_file.close()
	
#END with
labelled_file.close()

#print("initial training lines: " + str(training_line_count))
#print("final training lines: " + str(curr_training_line_count))
#print("initial testing lines: " + str(testing_line_count))
#print("final testing lines: " + str(curr_testing_line_count))
#print("total lines processed: " + str(line_count))